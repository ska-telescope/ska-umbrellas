SKA Umbrella charts for MVP Mid and Low
=======================================

This repository represents two set of working charts defined as umbrella charts which include for SKA-Low the following charts:
* tango-base, 
* webjive and 
* mccs-low.

For SKA-Mid the umbrella chart contains the following charts:
* etcd-operator,
* tango-base,
* cbf-proto,
* csp-proto,
* sdp-prototype,
* tmc-proto,
* oet,
* archiver,
* skuid,
* dsh-lmc-prototype,
* webjive and 
* logging.

All the above charts have been taken from skampi and from the [ska-low-mccs](https://gitlab.com/ska-telescope/ska-low-mccs) repository uploaded into the helm chart repository available on nexus. 

The values files demonstrate the [Helm](https://docs.helm.sh/using_helm/#installing-helm) [configuration](https://helm.sh/docs/chart_template_guide/subcharts_and_globals/) inheritance. There are few important concepts to consider about this topic:
* a chart is stand-alone that is a chart cannot depend on a parent chart;
* a sub-chart cannot access the values or its parent;
* a parent sub-chart can ovverride values for its sub-charts;
* all charts (parent and sub-chart) can access the global values. 

Because of the above considerations, the charts do not need to reflect the logical relationships between the components that they deploy. Besides, any (potential) sub-chart should never be self-contained for the purposes of testing because it will end up with circular dependencies specified in the chart hierarchy. This means that the umbrella charts must be seen as a logical container for deploying a set of SKA sub-charts that is customised for different purposes, in this case SKA-Low and SKA-Mid.

The values files of the two umbrella charts has many sub-chart section according to the number of sub-chart contained and one global section.

Makefile
--------
In the repository, it is included a Makefile for installing the charts into a k8s cluster. 

| Target          | Description                                                     |
|-----------------|-----------------------------------------------------------------|
| namespace_sdp   | create the kubernetes namespace for SDP dynamic deployments     |
| publish-chart   | publish chart in path UMBRELLA_CHART_PATH                       |
| reinstall-chart | reinstall the                                                   |
| uninstall-chart | uninstall the helm chart on the namespace KUBE_NAMESPACE        |
| upgrade-chart   | upgrade the helm chart on the namespace KUBE_NAMESPACE          |
| uninstall-chart | uninstall the ska-docker helm chart on the namespace ska-docker |
| upgrade-chart   | upgrade the ska-docker helm chart on the namespace ska-docker   |

Related stories
---------------
* https://jira.skatelescope.org/browse/ST-474
* https://jira.skatelescope.org/browse/SP-1241
* https://jira.skatelescope.org/browse/ADR-5
